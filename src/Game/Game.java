package Game;

/**
 * An implementation of noughts and crosses, with a simple AI opponent and two human player options
 * Each player can enter their name and preferred icon
 * Players can continue to play new games of noughts and crosses until they choose to exit
 * @author Sam Rosenberg
 */

public class Game {
    private Board board;
    private Player playerOne;
    private Player playerTwo;

    private void start(){
        initialiseGame();

        while (true) {
            System.out.println(board.printBoard());
            makeMove(playerOne); //impossible to win on the first move so no point checking; needs to outside loop to ensure player two isn't left trying to make an impossible move

            if (cycleTurnsToGameEnd()) {
                return; //players have quit
            }

            if (board.isFull(playerOne.getSymbol(), playerTwo.getSymbol())) {          //ensuring a draw is not declared when a game resets after a player win
                System.out.println("It's a draw!"); //method will exit if a player wins
                if (playerQuits()) {
                    return;
                } else {
                    board.reset();
                }
            }
        }
    }

    /**
     * Cycles between each player prompting them to take a turn. Has responsibility for notifying players of the winnner
     * Does not test for draws, so caller needs to consider this after a true return
     * @return false if players want to continue or game is drawn, true if a player wins and quits
     */
    private boolean cycleTurnsToGameEnd() {
        while (!board.isFull(playerOne.getSymbol(), playerTwo.getSymbol())) {
            makeMove(playerTwo);
            if (checkWinner(playerTwo)) {
                if (playerQuits()) {
                    return true;
                } else {
                    board.reset();
                    break;
                }
            }

            makeMove(playerOne);
            if (checkWinner(playerOne)) {
                if (playerQuits()) {
                    return true;
                } else {
                    board.reset();
                    break;
                }
            }
        }
        return false;
    }

    private boolean playerQuits() {
        System.out.println("Do you want to play again? (Yes/No)");
        String input = Keyboard.readInput().toLowerCase();
        if (input.equals("no") || input.equals("n") || input.equals("quit") || input.equals("q")) {
            return true;
        }
        Player temp = playerOne;
        playerOne = playerTwo;
        playerTwo = temp;
        return false;
    }

    private void initialiseGame() {
        board = new Board();
        namePlayers();
    }

    private void makeMove(Player player) {
        Player opponent;
        if (player.getName().equals(playerOne.getName())) {
            opponent = playerTwo;
        } else {
            opponent = playerOne;
        }

        if (player.isComputer()) {
            String selectedPosition = board.getNextBestPosition(player, opponent);
            System.out.println("The computer chooses position " + selectedPosition + ".");
            player.move(selectedPosition);
            System.out.println(board.printBoard());
        } else {
            humanMove(player, opponent);
        }
    }

    private void humanMove(Player player, Player opponent) {
        String message = "It's " + player.getName() + "'s move; enter the number of an available space";
        System.out.println(message + ", or 'h' for help.");

        String input = Keyboard.readInput();
        if (input.equals("h")) {
            System.out.println("A good next move might be space " + board.getNextBestPosition(player, opponent) + ".");
            System.out.println(message + ".");
            input = Keyboard.readInput();
        }

        while (!player.move(input)) {
            System.out.println("This is not a valid input; please enter one of the numbers displayed above");
            input = Keyboard.readInput();
        }

        System.out.println(board.printBoard());
    }

    private boolean checkWinner(Player player) {
        if (board.isWinner(player.getSymbol())) {
            declareWinner(player);
            return true;
        }
        return false;
    }

    private void namePlayers() {
        playerOne = createPlayer("1", "");
        playerTwo = createPlayer("2", playerOne.getSymbol());
    }

    private Player createPlayer(String playerId, String excludedSymbol) {
        System.out.println("Enter player " + playerId + "'s name, or type 'c' to enter a computer player");
        String playerName = Keyboard.readInput();

        if (playerName.equals("c")) {
            String playerSymbol;
            if (excludedSymbol.equals("X")) { playerSymbol = "O"; } else { playerSymbol = "X"; }
            return new Player(playerSymbol, "Computer " + playerId, board, true);
        }

        System.out.println("Thanks " + playerName + ". What symbol would you like to use? It can be any upper case letter");
        String playerSymbol = Keyboard.readInput();

        if (!isValidSymbolChoice(playerSymbol, excludedSymbol)) {
            while (!isValidSymbolChoice(playerSymbol, excludedSymbol)) {
                System.out.println("That was not a valid selection. Please try again");
                playerSymbol = Keyboard.readInput();
            }
        }

        return new Player(playerSymbol, playerName, board);
    }

    private Boolean isValidSymbolChoice(String symbol, String excludedSymbol) {
        return symbol.matches("[A-Z]") == !symbol.equals(excludedSymbol);
    }

    private void declareWinner(Player player){
        System.out.println(player.getName() + " wins!");
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }
}
