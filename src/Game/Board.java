package Game;

public class Board {
    private String board;
    int emptySpaces;

    public Board(){
        reset();
    }

    public void reset() {
        board = "123456789";
        emptySpaces = 9;
    }

    public Boolean isWinner(String symbol){
        return isWinner(board, symbol);
    }

    public Boolean isWinner(String board, String symbol){
        if (board.matches(symbol.repeat(3) + "......")) { return true; }
        if (board.matches("..." + symbol.repeat(3) + "...")) { return true; }
        if (board.matches("......" + symbol.repeat(3))) { return true; }
        if (board.matches(symbol + "..." + symbol + "..." + symbol)) { return true; }
        if (board.matches("." + symbol + ".." + symbol + ".." + symbol + ".")) { return true; }
        if (board.matches(".." + symbol + "." + symbol + "." + symbol + "..")) { return true; }
        if (board.matches(symbol + ".." + symbol + ".." + symbol + "..")) { return true; }
        return board.matches(".." + symbol + ".." + symbol + ".." + symbol);
    }

    public boolean isFull(String playerOneSymbol, String playerTwoSymbol){
        return emptySpaces == 0;
    }

    public void enterSelection(String position, String symbol){
        board = board.replace(position, symbol);
        emptySpaces--;
    }

    public boolean validInput(String input) {
        if (!input.matches("[1-9]")) {
            return false;
        } else {
            int position = Integer.parseInt(input);
            return board.substring(position - 1, position).equals(input);
        }
    }

    public String printBoard() {
        String humanFriendly = "\n";
        for (int i = 0; i < board.length(); i++) {

            humanFriendly = humanFriendly + " " + board.charAt(i);
            if ((i + 1) % 3 == 0) {
                humanFriendly = humanFriendly + "\n";
            }
        }
        return humanFriendly;
    }

    public String getNextBestPosition(Player player, Player opponent) {
        if (emptySpaces == 9) { //first move
            return "1";
        }
        if (emptySpaces == 8) {  //second move
            if (board.charAt(4) == '5') {
                return "5";
            } else {
                return "1";
            }
        }

        String opponentWin = "";
        String lastOpenCorner = "";
        String lastOpenPosition = "";
        String twoStepWinner = "";
        for (int i = 0; i < board.length(); i++) {
            if (!board.substring(i, i + 1).equals(i + 1 + "")) {
                continue;
            }
            if (isWinner(board.replace(i + 1 + "", player.getSymbol()), player.getSymbol())) {
                return i + 1 + "";
            }
            if (isWinner(board.replace(i + 1 + "", opponent.getSymbol()), opponent.getSymbol())) {
                opponentWin = i + 1 + "";
            }
            if (hasTwoStepWinner(board.replace(i + 1 + "", player.getSymbol()), player.getSymbol())) {
                twoStepWinner = i + 1 + "";
            }
            if (i == 2 || i == 6 || i == 8) {
                lastOpenCorner = i + 1 + "";
            }
            lastOpenPosition = i + 1 + "";
        }

        if (!opponentWin.isEmpty()) { return opponentWin; } //only return this if there are no winning positions

        if (!twoStepWinner.isEmpty()) {return twoStepWinner; } //only return if there are no winning positions for either player

        if (lastOpenCorner.isEmpty()) {
            return lastOpenPosition;
        } else {
            return lastOpenCorner;
        }
    }

    /**
     * Tests a hypothetical board state to see if it has any winning positions
     * @param board hypothetical board state to test
     * @param symbol is the symbol of the player to test for winner
     * @return true if winning move is present, otherwise fault
     */
    private boolean hasTwoStepWinner(String board, String symbol) {
        for (int i = 0; i < board.length(); i++) {
            if (!board.substring(i, i + 1).equals(i + 1 + "")) {
                continue;
            }
            if (isWinner(board.replace(i + 1 + "", symbol), symbol)) {
                return true;
            }
        }
        return false;
    }
}