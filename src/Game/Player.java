package Game;

public class Player {
    private String symbol;
    private String name;
    private Board board;
    private boolean isComputer;

    public Player(String symbol, String name, Board board) {
        this.symbol = symbol;
        this.name = name;
        this.board = board;
    }

    public Player(String symbol, String name, Board board, boolean isComputer) {
        this.symbol = symbol;
        this.name = name;
        this.board = board;
        this.isComputer = isComputer;
    }

    public String getSymbol(){
        return this.symbol;
    }

    public String getName(){
        return this.name;
    }

    public Boolean isComputer() { return this.isComputer; }

    public boolean move(String moveTo){
        if (!board.validInput(moveTo)) {
            return false;
        } else {
            board.enterSelection(moveTo, symbol);
            return true;
        }
    }
}
